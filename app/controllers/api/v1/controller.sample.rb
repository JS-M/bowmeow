class Api::V1::DreamsController < Api::V1::BaseController
  before_action -> { doorkeeper_authorize! :public }, only: :index
  before_action only: [:create, :update, :destroy] do
    doorkeeper_authorize! :admin, :write
  end

  private

  def dream_params
    params.require(:dream).permit(:date, :category, :description, :user_id)
  end

  def query_params
    # this assumes that a dream belongs to a user and has an :user_id
    # allowing us to filter by this
    params.permit(:user_id, :date, :category, :description)
  end

end