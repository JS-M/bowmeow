class Api::V1::TipsController < Api::V1::BaseController
  before_action :doorkeeper_authorize!

  # GET /api/v1/{plural_resource_name}
  def index
    plural_resource_name = "@#{resource_name.pluralize}"
    resources = resource_class.where(query_params).order(created_at: :desc)
                    .page(page_params[:page])
                    .per(page_params[:page_size])

    instance_variable_set(plural_resource_name, resources)
    respond_with instance_variable_get(plural_resource_name)
  end
end