class Api::V1::WelcomeController < Api::V1::BaseController
    before_action :doorkeeper_authorize!

    def index
        # result = EtorrentCrawler.new(87739).crawl_and_save
        result = ClienCrawler.new(307039).crawl_and_save

        render json: result
    end
end