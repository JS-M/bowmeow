module Api
  class BaseController < ActionController::Base
    include DeviseTokenAuth::Concerns::SetUserByToken

    respond_to :json

    protect_from_forgery with: :null_session

    def current_user
      if !doorkeeper_token || !doorkeeper_token[:resource_owner_id]
        return nil
      end
      @current_user ||= User.find(doorkeeper_token[:resource_owner_id])
    end
  end
end
