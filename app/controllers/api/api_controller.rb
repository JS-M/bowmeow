module API
	# ===================== Custom Errors =================================
	class ValidateError < StandardError
		attr_reader :errors
		def initialize(msg, errors)
			super(msg)
			@errors = errors
		end
	end
	class InvalidOwnerError < StandardError; end
	class DuplicateError < StandardError; end
	class RecordNotFoundError < StandardError; end


	class APIController < ActionController::Base
    include HeaderAttributeDecorator

    	respond_to :json

        before_action :action_log
	    before_action :set_locale
      before_action :header_attributes

		#1 using rescue_from
		rescue_from ActionView::MissingTemplate do |exception|
			# use exception.path to extract the path information
			# This does not work for partials
			raise exception if !defined?(@json_result)

			respond_with @json_result

			# after_action seems to be not called after a exception throwed
			validate_result if defined? API_SPEC_LOGGER
		end

		rescue_from API::ValidateError do |exception|
            render json:
                {
                    error: {
                        type: "validation_error",
                        message: exception.message ? exception.message : t('api.validation_error'),
                        fields: exception.errors
                    }
                }, status: :unprocessable_entity
		end

		rescue_from API::RecordNotFoundError do |exception|
            render json:
                {
                    error: {
                        type: "record_not_found_error",
                        message: exception.message ? exception.message : t('api.record_not_found_error')
                    }
                }, status: :unprocessable_entity
		end

		after_action :validate_result if defined? API_SPEC_LOGGER

		# override respond_with method to prevent the behavior which trying to
		# make redirect_uri when respond_with meets POST method
		# http://stackoverflow.com/questions/23582389/rails-nomethoderror-undefined-method-url-for-controller-i-cant-seem-to-res
		def respond_with(*args)
			super(*args, location: nil)
		end

		def current_user
			if !doorkeeper_token || !doorkeeper_token[:resource_owner_id]
				return nil
			end
		  	@current_user ||= User.find(doorkeeper_token[:resource_owner_id])
		end

		# around_action :respond_with_json

	    def set_locale
		    # Allow locale override (For now) based on params.
		    # Allow session locale
		    # Todo: default_locale based on server hostname
		    if SitePref.boolean("i18n.locale.enable")
			    I18n.locale = params[:locale] || session[:locale] || APP_CONFIG[:site_default_locale] || I18n.default_locale
			    session[:locale] = I18n.locale
		    else
			    I18n.locale = APP_CONFIG[:site_default_locale] || I18n.default_locale
			    session[:locale] = nil
		    end
      end

    def same_or_later_version_than?(target_version)
      return false if header_attributes[:app_version].nil? ||
          (AppVersion::ParsedVersion.parse(header_attributes[:app_version])<=>
              AppVersion::ParsedVersion.parse(target_version)) == -1
      true
    end

		protected
			def validate_result
				# spec = API::Helper.find_and_load_spec(params[:controller], params[:action])

				return if request.format.symbol == :html

				logger.debug "***Response Parameters***:" << response.body if Rails.env.development? || (Rails.env == "apitest")

				if Rails.env.development?
					API::Helper.fix_spec(params, response.body) if SitePref.integer("mobile_api.spec_fixer.activate") == 1 # This sitepref is only for developer's local server.
				end

				result = API::Helper.validate(params, response.body)

				if !result
					API_SPEC_LOGGER.warn("#{params[:controller]}##{params[:action]} failed")
					API_SPEC_LOGGER.warn("*" * 50)
				end
				logger.debug "******** #{result}"
			end

			# def respond_with_json
			# 	#2 using exception directly
			# 	begin
			# 		yield
			# 	rescue ActionView::MissingTemplate => e
			# 		if @json_result
			# 			respond_with @json_result
			# 		else
			# 			raise e
			# 		end
			# 	end
			# end

            def action_log
                # create a log to track user activity.

                # 1. Inspect request params contain a activity log param
                if params[:al]
                    # 2. Break down log param

                end
            end

    private
    def header_attributes
      @header_attributes = super
    end
	end

end