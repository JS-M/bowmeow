# # This file should be overwritten on deploy
# application_path = '/Users/munjuseong/projects/bowmeow'
# directory application_path
# environment 'development'
# # daemonize true
# port  3333
# pidfile "#{application_path}/tmp/pids/puma.pid"
# state_path "#{application_path}/tmp/pids/puma.state"
# stdout_redirect "#{application_path}/log/puma.stdout.log", "#{application_path}/log/puma.stderr.log"
# activate_control_app 'unix:///home/jobplanet/jobplanet/tmp/sockets/pumactl.sock', { no_token: true }
#
# threads 8,32
# workers 3
# preload_app!
# on_worker_boot do
#   ActiveSupport.on_load(:active_record) do
#     ActiveRecord::Base.establish_connection
#   end
#   logger_options = Rails.application.config.logstash
#   logger = LogStashLogger.new(logger_options)
#   Rails.application.config.logger = logger
# end
